FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get update -y

RUN apt-get dist-upgrade -y

RUN apt-get install apt-utils locales gnupg -y

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG en_US.utf8

RUN apt autoclean -y

RUN apt-get clean -y

RUN apt autoremove -y

RUN rm -rf /var/lib/apt/lists/*
